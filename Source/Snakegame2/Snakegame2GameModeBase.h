// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snakegame2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME2_API ASnakegame2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
